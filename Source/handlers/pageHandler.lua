include('common')

local Authorization = (include('controllers/authorization'))()
local HTTPHandler = include('class/HTTPHandler')

local Handler = HTTPHandler('/')

Handler:addRoute(
    '/login', function(request)
        if request.method == 'GET' then
            local params = request.params
            local action = params.action
            local LoggedIn, LoginResult = Authorization:Login(request['params'].username, request['params'].password)
            if LoggedIn then
                return {202,{user = username,logged_in = true}}
            else
                return {401,{user = username,logged_in = false, reason = LoginResult}}
            end
        elseif request.method == 'POST' then
            return HTTPCode(405, 'This route only Accepts GET requests')
        else
            return HTTPCode(400)
        end
    end
)

return Handler
