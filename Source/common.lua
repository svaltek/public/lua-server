function ScriptDir() return debug.getinfo(2).source:match('@?(.*/)') end

function include(filename)
    local server_dir = serverdir()
    local oldPackagePath = package.path
    package.path = server_dir .. '/' .. filename .. '.lua;' .. server_dir .. '/' .. filename .. ';' .. package.path
    local obj = require(filename)
    package.path = oldPackagePath
    if obj then
        return obj, 'success loading file from ' .. filename
    else
        return nil, 'Failed to Require file from path ' .. filename
    end
end

--- expand a string containing any `${var}` or `$var`.
--- Substitution values should be only numbers or strings.
--- @param s string the string
--- @param subst any either a table or a function (as in `string.gsub`)
--- @return string expanded string
function string_expand(s, subst)
    local res, k = s:gsub('%${([%w_]+)}', subst)
    if k > 0 then return res end
    return (res:gsub('%$([%w_]+)', subst))
end

---* bind an argument to a type and throw an error if the provided param doesnt match at runtime.
-- Note this works in reverse of the normal assert in that it returns nil if the argumens provided are valid
-- if not the it either returns true plus and error message , or if it fails to grab debug info just true.
--- @param idx number
-- positonal index of the param to bind
--- @param val any the param to bind
--- @param tp string the params bound type
--- @usage
-- local test = function(somearg,str,somearg)
-- if assert_arg(2,str,'string') then
--    return
-- end
--
-- test(nil,1,nil) -> Invalid Param in [test()]> Argument:2 Type: number Expected: string
function assert_arg(idx, val, tp)
    if type(val) ~= tp then
        local fn = debug.getinfo(2, 'n')
        local msg = 'Invalid Param in [' .. fn.name .. '()]> ' ..
                        string.format('Argument:%s Type: %q Expected: %q', tostring(idx), type(val), tp)
        local test = function() error(msg, 4) end
        local rStat, cResult = pcall(test)
        if rStat then return true end
    end
end

local function import_symbol(T, k, v, libname)
    local key = rawget(T, k)
    -- warn about collisions!
    if key and k ~= '_M' and k ~= '_NAME' and k ~= '_PACKAGE' and k ~= '_VERSION' then
        error('warning: \'%s.%s\' will not override existing symbol\n', libname, k)
        return
    end
    rawset(T, k, v)
end

local function lookup_lib(T, t)
    for k, v in pairs(T) do if v == t then return k end end
    return '?'
end

local already_imported = {}

---* take a table and 'inject' it into the local namespace.
--- @param t table
-- The Table
--- @param T  table
-- An optional destination table (defaults to callers environment)
function import(t, T)
    T = T or _G
    if type(t) == 'string' then t = require(t) end
    local libname = lookup_lib(T, t)
    if already_imported[t] then return end
    already_imported[t] = libname
    for k, v in pairs(t) do import_symbol(T, k, v, libname) end
end

-- @function compose
---* Create a function composition from given functions.
-- any further functions as arguments get added to composition in order
--- @param f1 function
-- the outermost function of the composition
--- @param f2 function
-- second outermost function of the composition
--- @return function the composite function
function compose(f1, f2, ...)
    if select('#', ...) > 0 then
        local part = compose(f2, ...)
        return compose(f1, part)
    else
        return function(...) return f1(f2(...)) end
    end
end

-- @function bind
---* Create a function with bound arguments ,
-- The bound function returned will call func() ,
-- with the arguments passed on to its creation .
-- If more arguments are given during its call, they are ,
-- appended to the original ones .
-- `...` the arguments to bind to the function.
--- @param func function
-- the function to create a binding of
--- @return function
-- the bound function
function bind(func, ...)
    local saved_args = {...}
    return function(...)
        local args = {table.unpack(saved_args)}
        for _, arg in ipairs({...}) do table.insert(args, arg) end
        return func(table.unpack(args))
    end
end

-- @function bind_self
---* Create f bound function whose first argument is t ,
--  Particularly useful to pass a method as a function ,
-- Equivalent to bind(t[k], t, ...) ,
-- `...` further arguments to bind to the function.
--- @param t table Binding
-- The table to be accessed
--- @param k any Key
-- The key to be accessed
--- @return function BoundFunc
-- The binding for t[k]
function bind_self(t, k, ...) return bind(t[k], t, ...) end

---* Create a function that returns the value of t[k] ,
-- | The returned function is Bound to the Provided Table,Key.
--- @param t table      table to access
--- @param k any        key to return
--- @return function returned getter function
function bind_getter(t, k)
    return function()
        if (not type(t) == 'table') then
            return nil, 'Bound object is not a table'
        elseif (t == {}) then
            return nil, 'Bound table is Empty'
        elseif (t[k] == nil) then
            return nil, 'Bound Key does not Exist'
        else
            return t[k], 'Fetched Bound Key'
        end
    end
end

---* Create a function that sets the value of t[k] ,
---| The returned function is Bound to the Provided Table,Key ,
---| The argument passed to the returned function is used as the value to set.
--- @param t table       table to access
--- @param k table       key to set
--- @return function     returned setter function
function bind_setter(t, k)
    return function(v)
        if (not type(t) == 'table') then
            return nil, 'Bound object is not a table'
        elseif (t == {}) then
            return nil, 'Bound table is Empty'
        elseif (t[k] == nil) then
            return nil, 'Bound Key does not Exist'
        else
            t[k] = v
            return true, 'Set Bound Key'
        end
    end
end

---* Create a function that returns the value of t[k] ,
---| The argument passed to the returned function is used as the Key.
--- @param t table       table to access
--- @return function     returned getter function
function getter(t)
    if (not type(t) == 'table') then
        return nil, 'Bound object is not a table'
    elseif (t == {}) then
        return nil, 'Bound table is Empty'
    else
        return function(k) return t[k] end
    end
end

---* Create a function that sets the value of t[k] ,
---| The argument passed to the returned function is used as the Key.
--- @param t table       table to access
--- @return function     returned setter function
function setter(t)
    if (not type(t) == 'table') then
        return nil, 'Bound object is not a table'
    elseif (t == {}) then
        return nil, 'Bound table is Empty'
    else
        return function(k, v)
            t[k] = v
            return true
        end
    end
end

function Class(base, new)
    local class = {} -- a new class instance
    if not new and type(base) == 'function' then
        new = base
        base = nil
    elseif type(base) == 'table' then
        -- our new class is a shallow copy of the base class!
        for i, v in pairs(base) do class[i] = v end
        class._base = base
    end
    -- the class will be the metatable for all its objects,
    -- and they will look up their methods in it.
    class.__index = class

    -- expose a constructor which can be called by <classname>(<args>)
    local mt = {}
    mt.__call = function(class_tbl, ...)
        local obj = {}
        setmetatable(obj, class_tbl)
        if class_tbl.new then
            class_tbl.new(obj, ...)
        else
            -- make sure that any stuff from the base class is initialized!
            if base and base.new then base.new(obj, ...) end
        end
        return obj
    end
    class.new = new
    class.is_a = function(self, klass)
        local m = getmetatable(self)
        while m do
            if m == klass then return true end
            m = m._base
        end
        return false
    end
    class.implement = function(self, ...)
        for _, cls in pairs({...}) do
            for k, v in pairs(cls) do if self[k] == nil and type(v) == 'function' then self[k] = v end end
        end
    end
    class.extend = function(self,name)
        local obj = {}
        for k, v in pairs(self) do if k:find('__') == 1 then obj[k] = v end end
        obj.__index = obj
        obj.super = self
        setmetatable(obj, self)
        return obj
    end
    setmetatable(class, mt)
    return class
end

---* Write file to Disk
---@param path string       path of file to Write, starts in Server root
---@param data any          File Contents to Write
---@return boolean,string   true,nil and a message
function WriteFile(path, data)
    local thisFile = assert(io.open(path, 'w'))
    if thisFile ~= nil then
        local fWritten = thisFile:write(data)
        thisFile:close()
        if fWritten ~= nil then
            return true, 'Success Writing File: <ServerRoot>/' .. path
        else
            return nil, 'Failed to Write Data to File: <ServerRoot>/' .. path
        end
    else
        return nil, 'Failed to Open file for Writing: <ServerRoot>/' .. path
    end
end

---* Read File from Disk
---@param path string      path of file to Write, starts in Server root
---@return boolean,any     true,nil and file content or message
function ReadFile(path)
    local thisFile, errMsg = io.open(path, 'r')
    if thisFile ~= nil then
        local fContent = thisFile:read('*all')
        thisFile:close()
        if fContent ~= '' or nil then
            return true, fContent
        else
            return nil, 'Failed to Read from File: ' .. path
        end
    else
        return nil, 'Error Opening file: ' .. path .. ' io.open returned:' .. errMsg
    end
end

---* Cleans Eccess quotes from input string
function clean_quotes(inputString)
    local result
    result = inputString:gsub('^"', ''):gsub('"$', '')
    result = result:gsub('^\'', ''):gsub('\'$', '')
    return result
end

function cmdSplit(pString, pPattern)
    local Table = {}
    local fpat = '(.-)' .. pPattern
    local last_end = 1
    local s, e, cap = pString:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= '' then table.insert(Table, cap) end
        last_end = e + 1
        s, e, cap = pString:find(fpat, last_end)
    end
    if last_end <= #pString then
        cap = pString:sub(last_end)
        table.insert(Table, cap)
    end
    return Table
end

function parseArgs(command)
    local cmdLine = cmdSplit(command, ' ')
    local cmdChunks = {}
    local ix = 0
    for iChunk, cmdChunk in pairs(cmdLine) do
        if ix ~= 1 then
            cmdChunks['0'] = cmdChunk
            ix = 1
        else
            local aKey, aValue = cmdChunk:match('([^,]+)=([^,]+)')
            if aValue ~= nil then cmdChunks[aKey] = aValue end
        end
    end
    return cmdChunks
end

function table_print(tt, indent, done)
    done = done or {}
    indent = indent or 0
    if type(tt) == 'table' then
        local sb = {}
        for key, value in pairs(tt) do
            table.insert(sb, string.rep(' ', indent)) -- indent it
            if type(value) == 'table' and not done[value] then
                done[value] = true
                table.insert(sb, key .. ' = {\n');
                table.insert(sb, table_print(value, indent + 2, done))
                table.insert(sb, string.rep(' ', indent)) -- indent it
                table.insert(sb, '}\n');
            elseif 'number' == type(key) then
                table.insert(sb, string.format('"%s"\n', tostring(value)))
            else
                table.insert(sb, string.format('%s = "%s"\n', tostring(key), tostring(value)))
            end
        end
        return table.concat(sb)
    else
        return tt .. '\n'
    end
end
