local Auth = Class {}

---* Add a new User
function Auth:AddUser(username, password, email)
    if HasUser(username) then return false, 'username unavailable' end
    AddUser(username, HashPassword(username, password), email)
    if HasUser(username) then
        return true
    else
        return nil, 'Failed to Create User: ' .. username
    end
end

function Auth:RemoveUser(username)
    --- check user doesnt allready exist
    if not HasUser(username) then return false, 'user not found' end
    -- remove user
    RemoveUser(username)
    -- validate
    if HasUser(username) then
        return nil, 'failed to remove user: ' .. username
    else
        return true
    end
end

function Auth:Login(username, password)
    if not IsLoggedIn(username) then
        if CorrectPassword(username,password) then
            Login(username)
            if IsLoggedIn() then
                return true, username .. ' logged in'
            else
                return nil, 'failed to login'
            end
        else
            return false, 'invalid user details'
        end
    else
        return nil, 'user allready logged in'
    end
end

return Auth